@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-3">
                <div class="col ps-4">      
                    <div class="row align-items-md-stretch mt-4">
                        <div class="col">
                            <div class="p-3 text-white bg-dark rounded-3 shadow">
                                <h3>Bienvenido al Sistema!</h3>
                                <p>Aqui puedes colocar un mensaje.</p>
                            </div>
                        </div>
                        <div class="col">
                            <div class="p-3 bg-white border rounded-3 shadow" style="height: 100%;">
                                <h3>Otro mensaje</h3>
                                <p>Aquí también.</p>
                            </div>
                        </div> 
                    </div>
                    
                    <div class="row mt-4">
                        <div class="col-lg-6">
                            <div class="card mb-3 shadow">
                                <div class="card-header bg-transparent d-flex justify-content-between"><span><i class="bi bi-megaphone me-2"></i>Anuncios</span></div>
                                    <div class="card-body p-0 text-dark">
                                        <div>
                                            <div class="accordion" id="accordionNotice">
                                                @foreach ($notices as $notice)    
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingOne{{$notice->id}}">
                                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne{{$notice->id}}" aria-expanded="true" aria-controls="collapseOne" >
                                                            Fecha de publicación: {{$notice->created_at}}
                                                        </button>
                                                    </h2>
                                                    <div id="collapseOne{{$notice->id}}" class="accordion-collapse collapse" aria-labelledby="headingOne{{$notice->id}}" data-bs-parent="#accordionExample">
                                                        <div class="accordion-body overflow-auto">{!!Purify::clean($notice->notice)!!}</div>
                                                        <form action="{{ route('notices.destroy', $notice->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <div class="text-end">
                                                        <button type="submit" title="Eliminar" class="btn btn-outline-danger btn-sm"><i class="bi bi-trash"></i></button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                </div> 
                                                @endforeach  
                                                @if(count($notices) < 1)
                                                <div class="p-3">No hay anuncios</div>
                                                @endif                                     
                                            </div>                                           
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- @i('layouts.footer') -->
    </div>
</div>
@endsection                                       