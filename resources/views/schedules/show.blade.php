@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-calendar4-range"></i> Horario</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Lista de Usuarios</li>
                        </ol>
                    </nav>
                    @include('messages')
                    @php
                        function getDayName($weekday) {
                            if($weekday == 1) {
                                return "LUNES";
                            } else if($weekday == 2) {
                                return "MARTES";
                            } else if($weekday == 3) {
                                return "MIERCOLES";
                            } else if($weekday == 4) {
                                return "JUEVES";
                            } else if($weekday == 5) {
                                return "VIERNES";
                            } else {
                                return "SIN DIA";
                            }
                        }
                    @endphp
                    @if(count($schedules) > 0)
                    <div class="bg-white p-3 border shadow-sm">
                        <table class="table table-bordered text-center">
                        </thead>
                            <tbody>
                            @foreach($schedules as $day => $subjects)
                                    <tr><th>{{getDayName($day)}}</th>
                                        @php
                                            $subjects = $subjects->sortBy('start');
                                        @endphp
                                        @foreach($subjects as $subject)
                                            <td>
                                                <span>{{$subject->subjects->subject_name}}</span>
                                                <div>{{$subject->start}} - {{$subject->end}}</div>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div class="p-3 bg-white border shadow-sm">Sin Horario.</div>
                    @endif
                </div>
            </div>
            <!--@i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection