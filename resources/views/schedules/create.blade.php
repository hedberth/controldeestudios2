@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Crear Horario</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Horarios</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="row">
                        <div class="col-md-5 mb-4">
                            <div class="p-3 border bg-light shadow">
                                <form action="{{route ('schedule.store')}}" method="POST">
                                    @csrf
                                    <div class="mb-2">
                                        <p>Carrera:<sup><i class="bi bi-asterisk text-primary"></i></sup></p>
                                        <select id='career_id' class="form-select" aria-label=".form-select" name="career_id" required>
                                                <option value="">Selecciona una carrera</option>
                                            @foreach ($careers as $career)
                                                <option value="{{$career->id}}">{{$career->career_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <p>Asignar a una Sección:<sup><i class="bi bi-asterisk text-primary"></i></sup></p>
                                        <select id="section_id" class="form-select" aria-label=".form-select" name="section_id" required>
                                            
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <p>Asignar a una Carrera:<sup><i class="bi bi-asterisk text-primary"></i></sup></p>
                                        <select id="subject_id" class="form-select" aria-label=".form-select" name="subject_id" required>
                                                
                            
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <p>Día:<sup><i class="bi bi-asterisk text-primary"></i></sup></p>
                                        <select id="weekday" class="form-select" aria-label=".form-select" name="weekday" required>
                                                <option value="">Selecciona un Dia de la semana</option>
                                                <option value="1">Lunes</option>
                                                <option value="2">Martes</option>
                                                <option value="3">Miercoles</option>
                                                <option value="4">Jueves</option>
                                                <option value="5">Viernes</option>
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <label for="start" class="form-label">Comienza: </label>
                                        <input type="time" class="form-control" id="start" name="start"  required>
                                    </div>
                                    <div class="mb-2">
                                        <label for="end" class="form-label">Termina: </label>
                                        <input type="time" class="form-control" id="end" name="end"  required>
                                    </div>
                                    <button class="btn btn-sm btn-outline-success" type="submit"><i class="bi bi-check2"></i> Crear</button>
                                </form>
                                <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
                                <script>
                                    $('#career_id').change(function(){
                                        
                                        let careerID = $(this).val();

                                        if (careerID){

                                            $.ajax({
                                                type: 'GET',
                                                url: "{{ url('getSection') }}?career_id=" + careerID,
                                                success: function(res) {

                                                    if (res) {
                                                        $("#section_id").empty();
                                                        $("#section_id").append('<option>Seleciona una Sección</option>');
                                                        $.each(res, function(key, value) {
                                                            $("#section_id").append('<option value="' + key + '">' + value + '</option>');

                                                           
                                                        });

                                                    } else {

                                                        $("#section_id").empty();
                                                    }
                                                }
                                            });
                                        } 

                                    });
                                    $('#career_id').change(function(){
                                        
                                        let careerID = $(this).val();

                                        if (careerID){

                                            $.ajax({
                                                type: 'GET',
                                                url: "{{ url('getSubject') }}?career_id=" + careerID,
                                                success: function(res) {

                                                    if (res) {
                                                        $("#subject_id").empty();
                                                        $("#subject_id").append('<option>Seleciona una Materia</option>');
                                                        $.each(res, function(key, value) {
                                                            $("#subject_id").append('<option value="' + key + '">' + value + '</option>');

                                                           
                                                        });

                                                    } else {

                                                        $("#subject_id").empty();
                                                    }
                                                }
                                            });
                                        } 

                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection