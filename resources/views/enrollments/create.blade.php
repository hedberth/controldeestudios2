@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Inscribir Materias {{auth::user()->first_name}}</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Inscripción</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="row">
                        <div class="col-md-5 mb-4">
                            <div class="p-3 border bg-light shadow">
                                <form action="{{route('enrollment.store', auth()->id())}}" method="POST">
                                    @csrf
                                    <div class="mb-3">
                                        <p>Inscribir Materias:<sup><i class="bi bi-asterisk text-primary"></i></sup></p>
                                            @foreach ($subjects as $subject)
                                                @if (auth::user()->sections->semester_id == $subject->semester_id)
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" name="subject_id[]" value="{{$subject->id}}"> <strong>{{$subject->subject_name}}</strong>
                                                </div>
                                                @endif
                                            @endforeach

                                            <input type="hidden" id="user_id" name="user_id" value="{{auth()->id()}}">

                                            <input type="hidden" id="section_id" name="section_id" value="{{auth::user()->section_id}}">

                                            
                                        </select>           
                                    </div>
                                    <button type="submit" class="mt-3 btn btn-sm btn-outline-success"><i class="bi bi-check2"></i> Inscribir</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection