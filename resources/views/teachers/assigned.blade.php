@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Asignar Profesor</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Asignar Profesor a una materia</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="row">
                        <div class="col-md-5 mb-4">
                            <div class="p-3 border bg-light shadow">
                                <form action="{{ route ('teacher.store')}}" method="POST">
                                    @csrf
                                    <div class="mb-2">
                                        <p>Asignar Profesor:<sup><i class="bi bi-asterisk text-primary"></i></sup></p>
                                        <select class="form-select" aria-label=".form-select" name="user_id" required>
                                                <option value="">Seleccionar un Profesor</option>
                                            @foreach ($users as $user)
                                            @if ($user->role == 'teacher')
                                                <option value="{{$user->id}}">{{$user->first_name}} {{$user->last_name}} CI: {{$user->ci}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <p>Asignar a una Carrera:<sup><i class="bi bi-asterisk text-primary"></i></sup></p>
                                        <select id="career_id" class="form-select" aria-label=".form-select" name="career_id" required>
                                                <option value="">Seleccionar una Carrera</option>
                                            @foreach ($careers as $career)
                                                <option value="{{$career->id}}">{{$career->career_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-2">
                                        <p>Asignar a una Sección:<sup><i class="bi bi-asterisk text-primary"></i></sup></p>
                                        <select id="section_id" class="form-select" aria-label=".form-select" name="section_id" required>
                                            
                                        </select>
                                    </div>
                                    <button class="btn btn-sm btn-outline-success" type="submit"><i class="bi bi-check2"></i> Seguir</button>
                                </form>
                                <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
                                <script>
                                    $('#career_id').change(function(){
                                        
                                        let careerID = $(this).val();

                                        if (careerID){

                                            $.ajax({
                                                type: 'GET',
                                                url: "{{ url('getSection') }}?career_id=" + careerID,
                                                success: function(res) {

                                                    if (res) {
                                                        $("#section_id").empty();
                                                        $("#section_id").append('<option>Seleciona una Sección</option>');
                                                        $.each(res, function(key, value) {
                                                            $("#section_id").append('<option value="' + key + '">' + value + '</option>');

                                                           
                                                        });

                                                    } else {

                                                        $("#section_id").empty();
                                                    }
                                                }
                                            });
                                        } 

                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection