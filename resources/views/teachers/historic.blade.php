@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-person-lines-fill"></i>Historico del Profesor {{$user->first_name}}</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Lista de Estudiantes</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="mb-4 mt-4">
                        <div class="bg-white border shadow-sm p-3 mt-4">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th scope="col">Carrera</th>
                                        <th scope="col">Materia</th>
                                        <th scope="col">Sección</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($assignedTeachers as $assignedTeacher)
                                        @if ($user->id == $assignedTeacher->user_id)
                                        <tr>
                                            <td>{{$assignedTeacher->careers->career_name}}</td>
                                            <td>{{$assignedTeacher->subjects->subject_name}}</td>
                                            <td>{{$assignedTeacher->sections->section_name}}</td>
                                            
                                        </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--@i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection
