@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Asignar Profesor</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Asignar Profesor a una materia</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="row">
                        <div class="col-md-5 mb-4">
                            <div class="p-3 border bg-light shadow">
                                <form action="{{ route ('teacher.storeTwo', $assignedTeacher->id)}}" method="POST">
                                    @csrf
                                    <div class="mb-2">
                                        <p>Asignar Profesor:<sup><i class="bi bi-asterisk text-primary"></i></sup></p>
                                        <select class="form-select" aria-label=".form-select" name="subject_id" required>
                                                <option value="">Seleccionar una Materia</option>
                                            @foreach ($subjects as $subject)
                                                @if ($assignedTeacher->career_id == $subject->career_id && $subject->semester_id == $assignedTeacher->sections->semester_id)
                                                <option value="{{$subject->id}}">{{$subject->subject_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <button class="btn btn-sm btn-outline-success" type="submit"><i class="bi bi-check2"></i> Guardar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection