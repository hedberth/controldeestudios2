@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-person-lines-fill"></i> Lista de Estudiantes de {{$subject->subject_name}}</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Lista de Estudiantes</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="mb-4 mt-4">
                        <div class="bg-white border shadow-sm p-3 mt-4">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Apellido</th>
                                        <th scope="col">Correo</th>
                                        <th scope="col">Cédula</th>
                                        <th scope="col">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($enrollments as $enrollment)
                                    @if ($enrollment->subject_id == $subject->id && $enrollment->section_id == $section->id)
                                    <tr>
                                        <td>{{$enrollment->users->first_name}}</td>
                                        <td>{{$enrollment->users->last_name}}</td>
                                        <td>{{$enrollment->users->email}}</td>
                                        <td>{{$enrollment->users->ci}}</td>
                                        <td>
                                            <div class="btn-group" role="group">
                                                <a href="{{ route('users.show', $enrollment->user_id)}}" role="button" class="btn btn-sm btn-outline-success"><i class="bi bi-eye"></i> Perfil</a>
                                                
                                                <a href="{{ route('user.record', $enrollment->user_id)}}" role="button" class="btn btn-sm btn-outline-success"><i class="bi bi-pen"></i> notas</a>
                                            </div>
                                        </td>
                                    </tr>
                                   @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--@i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection
