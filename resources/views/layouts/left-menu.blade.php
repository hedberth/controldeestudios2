<div class="col-xs-1 col-sm-1 col-md-1 col-lg-2 col-xl-2 col-xxl-2 border-rt-e6 px-0" id="left-menu">
    <div class="d-flex flex-column align-items-center align-items-sm-start">
        <ul class="nav flex-column pt-2 w-100">
            <li class="nav-item">
                <a class="nav-link {{ request()->is('home')? 'active' : '' }}" href="{{url('home')}}"><i class="ms-auto bi bi-grid"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Menú Principal</span></a>
            </li>
            @can('isAdmin')
            <li class="nav-item">
                <a type="button" href="#career-submenu" data-bs-toggle="collapse" class="d-flex nav-link {{ request()->is('career*')? 'active' : '' }}"><i class="bi bi-clouds"></i> <span class="ms-2 d-inline d-sm-none d-md-none d-xl-inline">Carreras</span>
                    <i class="ms-auto d-inline d-sm-none d-md-none d-xl-inline bi bi-chevron-down"></i>
                </a>
                <ul class="nav collapse {{ request()->is('career*')? 'show' : 'hide' }}" id="career-submenu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('careers.create') }}"><i class="bi bi-cloud-upload"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Crear Carrera</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route ('careers.index') }}"><i class="bi bi-cloud-check"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Ver Carreras</span></a>
                    </li>
                </ul>
            </li>
            
            <li class="nav-item">
                <a class="nav-link {{ request()->is('subjects/create')? 'active' : '' }}" href="{{ route('subjects.create') }}"><i class="bi bi-journals"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Materias</span></a>
            </li>

            <li class="nav-item">
                <a type="button" href="#semester-submenu" data-bs-toggle="collapse" class="d-flex nav-link {{ request()->is('semester*')? 'active' : '' }}"><i class="bi bi-back"></i> <span class="ms-2 d-inline d-sm-none d-md-none d-xl-inline">Semestres</span>
                    <i class="ms-auto d-inline d-sm-none d-md-none d-xl-inline bi bi-chevron-down"></i>
                </a>
                <ul class="nav collapse {{ request()->is('semester*')? 'show' : 'hide' }}" id="semester-submenu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('semesters.create') }}"><i class="bi bi-ui-checks-grid"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Crear Semestre</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route ('semesters.index') }}"><i class="bi bi-ui-checks"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Ver Semestres</span></a>
                    </li>
                </ul>
            </li>
            
            <li class="nav-item">
                <a class="nav-link {{ request()->is('notices/create')? 'active' : '' }}" href="{{ route('notices.create')}}"><i class="bi bi-megaphone"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Anuncios</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ request()->is('sections/create')? 'active' : '' }}" href="{{ route('sections.create') }}"><i class="bi bi-card-checklist"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Secciones</span></a>
            </li>

            <li class="nav-item border-bottom">
                <a class="nav-link {{ request()->is('schedules/create')? 'active' : '' }}" href="{{ route('schedule.create')}}"><i class="bi bi-calendar4-range"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Horarios</span></a>
            </li>
            @endcan

            @can('isStudent')
            <li class="nav-item border-bottom">
                <a class="nav-link {{ request()->is('schedules/show')? 'active' : '' }}" href="{{ route('schedule.show',['career'=> auth::user()->career_id, 'section'=> auth::user()->section_id])}}"><i class="bi bi-calendar4-range"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Horario</span></a>
            </li>
            @endcan
            <!-- PARTE BAJA -->

            @can('isAdmin')
            <li class="nav-item">
                <a type="button" href="#user-submenu" data-bs-toggle="collapse" class="d-flex nav-link {{ request()->is('user*')? 'active' : '' }}"><i class="bi bi-person-lines-fill"></i> <span class="ms-2 d-inline d-sm-none d-md-none d-xl-inline">Usuarios</span>
                    <i class="ms-auto d-inline d-sm-none d-md-none d-xl-inline bi bi-chevron-down"></i>
                </a>
                <ul class="nav collapse {{ request()->is('user*')? 'show' : 'hide' }}" id="user-submenu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('users.create')}}"><i class="bi bi-person-plus"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Añadir Usuarios</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('users.index')}}"><i class="bi bi-person-video2"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Ver Usuarios</span></a>
                    </li>
                </ul>
            </li>
            @endcan
            <li class="nav-item">
                <a class="nav-link {{ request()->is('enrollments/create')? 'active' : '' }}" href="{{ route ('enrollment.create', auth()->id() )}}"><i class="bi bi-person-plus"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Inscripción</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ request()->is('teachers/assigned')? 'active' : '' }}" href="{{ route ('teacher.assigned')}}"><i class="bi bi-person-plus-fill"></i><span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Profesores</span></a>
            </li>

            @can('isTeacher')
            <li class="nav-item">
                <a class="nav-link {{ request()->is('teachers/view/{user}/historic')? 'active' : '' }}" href="{{ route ('teacher.historic', auth()->id())}}"><i class="bi bi-person-plus-fill"></i><span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Historico</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ request()->is('teachers/view/{user}/section')? 'active' : '' }}" href="{{ route ('teacher.section', auth()->id())}}"><i class="bi bi-person-plus-fill"></i><span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Secciones</span></a>
            </li>
            @endcan

            <li class="nav-item">
                <a class="nav-link {{ request()->is('subjects/create')? 'active' : '' }}" href="{{ route ('constancia.pdf')}}"><i class="bi bi-journals"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Constancia</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link disabled" href="#"><i class="bi bi-tools"></i> <span class="ms-1 d-inline d-sm-none d-md-none d-xl-inline">Academic</span></a>
            </li>
            
        </ul>
    </div>
</div>