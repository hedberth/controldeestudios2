<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Control') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js','resources/css/app.css'])
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('home') }}">{{ config('app.name', 'Laravel') }}</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Iniciar') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Registro') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->first_name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route ('users.show', auth()->id())}}"><i class="bi bi-person-fill"></i> Mi perfil</a>
                                <a class="dropdown-item" href="{{ route ('user.viewChange')}}"><i class="bi bi-person-fill"></i> Cambiar contraseña</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="bi bi-door-open-fill"></i>
                                        {{ __('Salir') }}
                                    </a>
                                   

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @if(Auth::check())
        <div class="container__menu">
            <div class="menu">
                <nav>
                    <ul class="botones_menu">
                        <li><a href="{{route ('home')}}"></a></li>
                        <li>
                            <a href="{{ route ('careers.index')}}">Carreras</a>
                            <ul>
                                <li><a href="{{ route ('careers.create')}}">Crear</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{route ('semesters.index')}}">Semestres</a>
                            <ul>
                                <li><a href="{{ route ('semesters.create')}}">Crear Semestres</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{route ('sections.create')}}">Secciones</a>
                        </li>
                        <li>
                            <a href="{{route ('subjects.create')}}">Materias</a>
                        </li>
                        <li>
                            <a href="{{route ('notices.create')}}">Anuncios</a>
                        </li>
                        <li>
                            <a href="{{route ('schedule.create')}}">Horarios</a>
                        </li>
                        <li>
                            <a href="{{route ('semesters.index')}}">Usuarios</a>
                            <ul>
                                <li><a href="{{ route ('users.create')}}">Añadir</a></li>
                                <li><a href="{{ route ('users.index')}}">Administradores</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ route ('teacher.assigned')}}">Profesores</a>
                        </li>
                        <li>
                            <a href="{{ route ('enrollment.create', auth()->id() )}}">Inscripción</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        @endif
        <main class="py-2">
            @yield('content')
        </main>
    </div>
</body>
</html>
