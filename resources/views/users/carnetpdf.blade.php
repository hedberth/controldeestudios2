<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: sans-serif;
}

body {
    /* display: flex;
    align-items: center;
    justify-content: center; */
    min-height: 100vh;
    background: #e3d8d8;
}

.titulo{
    text-align: center;
    text-justify: auto;
    width: 550px;
    margin: auto;
    
}

.close{
    display: flex;
    align-items: center;
    justify-content: center;
}

.card {
    position: relative;
    width: 250px;
    height: 350px;
    border-radius: 10px;
    box-shadow: 2px 3px 5px rgba(73, 69, 52, 0.4);
    margin: 50px 5px;
}

.card .content {
    position: relative;
    z-index: 100;
    width: 100%;
    height: 100%;
    border-radius: 10px;
    display: flex;
    flex-direction: column;
    overflow: hidden;  
    text-align: center;  
    padding: 20px;
    background: #fff;
    border: dashed;
}

.card .content .img {
    height: 50%;
    margin-bottom: 20px;
}

.card .content .img img {
    position: relative;
    width: 150px;
    height: 150px;
    border-radius: 50%;
    object-fit: cover;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.8);
}

.card .content span {
    position: absolute;
    width: 350px;
    height: 200px;
    background: linear-gradient(to right, #5e2066, #b332c7);
    transform: rotate(-35deg);
    top: -50px;
    left: -100px;
}

.card .content h4 {
    font-size: 18px;
    color: #1a1919;
    margin-bottom: 5px;
}

.card .content h6 {
    font-size: 13px;
    color: #5e2066;
}

.card .content  {
    font-size: 13px;
    color: #1a161f;
    margin-top: 10px;
}



    </style>
</head>
<body>
    <br>
    <br>
    <div class="titulo">
    <h1>Carnet universitario</h1>
    <br>
    <br>
    <p>Ahora eres portador de un carnet de identidicación felicidades, es un gusto para nosotros que formes parte de rata gran institución</p>
    </div>
    <div class="close">
    <div class="card">
        <div class="content">
            <span></span>
            <div class="img">
                <img src="{{ public_path() . '/image/steve-jobs.jpeg'}}" alt="">
            </div>
            <h4>{{auth()->user()->first_name}} {{auth()->user()->last_name}}</h4>
            <h6>{{auth()->user()->ci}}</h6>
            <h6>Carrera: {{auth()->user()->careers->career_name}}</h6>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor quae accusamus deserunt ducimus</p>
        </div>
        <div class="links">
            <a href="#"><i class="fa-brands fa-facebook-f"></i></a>
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-github"></i></a>
            <a href="#"><i class="fa-brands fa-youtube"></i></a>
        </div>
    </div>
    <div class="card">
        <div class="content">
            <div class="img">
                <img src="{{ public_path() . '/image/logo2.png'}}" alt="">
            </div>
            <h4>Parte lateral</h4>
            <h6>Ayudando a tu desarrollo </h6>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor quae accusamus deserunt ducimus</p>
        </div>
        <div class="links">
            <a href="#"><i class="fa-brands fa-facebook-f"></i></a>
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-github"></i></a>
            <a href="#"><i class="fa-brands fa-youtube"></i></a>
        </div>
    </div>

    </div>
</body>
</html>