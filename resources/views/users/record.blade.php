@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-person-lines-fill"></i> Record academico de {{$user->first_name}}</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Lista de Estudiantes</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="mb-4 mt-4">
                        <div class="bg-white border shadow-sm p-3 mt-4">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th scope="col">Carrera</th>
                                        <th scope="col">Materia</th>
                                        <th scope="col">Semestre</th>
                                        <th scope="col">Nota</th>
                                        <th scope="col">Aprovado</th>
                                        <th scope="col">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($enrollments as $enrollment)
                                        @if ($user->id == $enrollment->user_id)
                                        <tr>
                                            <td>{{$enrollment->users->careers->career_name}}</td>
                                            <td>{{$enrollment->subjects->subject_name}}</td>
                                            <td>{{$enrollment->subjects->semesters->semester_name}}</td>
                                            <td>{{$enrollment->mark}}</td>
                                            @if($enrollment->mark == null)
                                            <td></td>
                                            @elseif($enrollment->mark >='10')
                                            <td><i id="aprobado" class="bi bi-check-square-fill"></i></td>
                                            @else
                                            <td><i id="reprobado" class="bi bi-x-square-fill"></i></td>
                                            @endif
                                            <td>
                                                <div class="btn-group" role="group">
                                                    <a href="{{ route('enrollment.edit', $enrollment->id) }}" role="button" class="btn btn-sm btn-info"><i class="bi bi-file-earmark-check"></i></a>    
                                                </div>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--@i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection
