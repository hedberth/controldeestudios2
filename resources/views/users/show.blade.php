@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-person-lines-fill"></i> Perfil de {{$user->first_name}}</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('users.index')}}">Lista de Usuarios</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Perfil</li>
                        </ol>
                    </nav>
                    <div class="mb-4">
                        <div class="row">
                            <div class="col-sm-4 col-md-3">
                                <div class="card bg-light">
                                    <div class="px-5 pt-2">
                                        
                                    </div>
                                    <div class="card-body">
                                        <h1 style="text-align:center"><i class="bi bi-person-circle"></i></h1>
                                        <h5 style="text-align:center" class="card-title">{{$user->first_name}} {{$user->last_name}}</h5>
                                        
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Género: {{ucfirst($user->gender)}}</li>
                                        <li class="list-group-item">Teléfono: {{$user->phone}}</li>
                                        {{-- <li class="list-group-item"><a href="#">View Marks &amp; Results</a></li> --}}
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="p-3 mb-3 border rounded bg-white">
                                    <h6>Información del Estudiante</h6>
                                    <table class="table table-responsive mt-3">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Primer Nombre:</th>
                                                <td>{{$user->first_name}}</td>
                                                <th>Apellido:</th>
                                                <td>{{$user->last_name}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Correo:</th>
                                                <td>{{$user->email}}</td>
                                                <th>Birthday:</th>
                                                <td>{{$user->birthdate}}</td>
                                            </tr>
                                           
                                            <tr>
                                                <th scope="row">Dirección:</th>
                                                <td>{{$user->address}}</td>
                                                <th scope="row">Género:</th>
                                                <td colspan="3">{{ucfirst($user->gender)}}</td>
                                            </tr>
                                            <tr>
                                            @if ($user->role == 'student')
                                                <th scope="row">Carrera</th>
                                                <td>{{$user->careers->career_name}}</td>
                                            @endif
                                            @if ($user->role !== 'student')
                                                <th scope="row">Rol</th>
                                                <td>{{$user->role}}</td>
                                            @endif
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--@i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection
