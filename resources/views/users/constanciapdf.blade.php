<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <br>
    <br>
    <div>
        <img src="{{ public_path() . '/image/logo2.png'}}" width="100">
        <div style="text-align: center;">
        <h1>CONTANCIA DE ESTUDIOS</h1>
        <strong><p>El intitunto nacional tecnologico "IDEA"</p></strong> 
        <h3 >Hace constar:</h3>
        </div>
        <div style="text-align: justify; width: 60%;  margin: auto  ">
        <p> Que el alumno:<strong> {{auth()->user()->first_name}} {{auth()->user()->last_name}} Titular de la cedula de identidad: {{auth()->user()->ci}}</strong> alumno del instituto tecnologico del IDEA
        quien cursa sus estudios en la carrera {{auth()->user()->careers->career_name}} nombre de la carreara la cual tiene una duracion
        de 9 semestres equivalentes a (4 años y medio) y actualmente se encuentra en el  {{auth()->user()->sections->semesters->semester_name}}</p>
        </div>
        <div style="text-align: justify; width: 60%;  margin: auto  ">
        <p><strong>se expide la presente constancia a nombre del interesado(a) para los fines que se emite conveniente se brinda la informacion para fines academicos</strong></p>
        </div>
        <br>
        <br>
        <div style="text-align: center;">
        <p>__________________</p>
        <p><strong>firma y sello</strong></p>
        </div>
        <br>
        <p style="float: right; margin: 100px;">solicitada en la fecha {{$date->format('d-m-Y')}}</p>
    </div>
</body>
</html>