@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Editar Usuario</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Editar Usuario</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="mb-4">
                            <div class="p-3 border bg-light shadow">
                                <form action="{{ route('users.update', $user->id)}}" method="POST">
                                    @csrf
                                    @method ('put')
                                    <div class="row g-3">
                                        <div class="col-md-3">
                                            <label for="first_name" class="form-label">Nombre<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Nombre"  value="{{$user->first_name}}" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="last_name" class="form-label">Apellido<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Apellido"  value="{{$user->last_name}}" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="birthdate" class="form-label">Fecha de Nacimiento<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="date" class="form-control" id="birthday" name="birthdate"  value="{{$user->birthdate}}" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="ci" class="form-label">Cédula<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="text" class="form-control" id="ci" name="ci" placeholder="12345678"  value="{{$user->ci}}" required>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="email" class="form-label">Correo<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="usuario@ejemplo.com"  value="{{$user->email}}" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="gender" class="form-label">Género<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <select id="gender" class="form-select" name="gender" required>
                                                <option value="Male" {{old('gender') == 'male' ? 'selected' : ''}}>Masculino</option>
                                                <option value="Female" {{old('gender') == 'female' ? 'selected' : ''}}>Femenimo</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="address" class="form-label">Dirección<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="text" class="form-control" id="address" name="address" placeholder="Avenida López"  value="{{$user->address}}" required>
                                        </div>   
                                        <div class="col-md-3">
                                            <label for="phone" class="form-label">Teléfono<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="text" class="form-control" id="phone" name="phone" placeholder="0412......"  value="{{$user->phone}}" required>
                                        </div>
                                       
                                    </div>
                                    <button type="submit" class="mt-3 btn btn-sm btn-outline-success"><i class="bi bi-check2"></i> Guardar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection