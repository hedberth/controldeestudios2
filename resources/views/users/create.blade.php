@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Añadir Usuario</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Añadir Usuario</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="mb-4">
                            <div class="p-3 border bg-light shadow">
                                <form action="{{ route('users.store')}}" method="POST">
                                    @csrf
                                    <div class="row g-3">
                                        <div class="col-md-3">
                                            <label for="first_name" class="form-label">Nombre<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Nombre" required value="{{old('first_name')}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="last_name" class="form-label">Apellido<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Apellido" required value="{{old('last_name')}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="birthdate" class="form-label">Fecha de Nacimiento<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="date" class="form-control" id="birthday" name="birthdate" required value="{{old('birthdate')}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="ci" class="form-label">Cédula<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="text" class="form-control" id="ci" name="ci" placeholder="12345678" required value="{{old('ci')}}">
                                        </div>
                                        <div class="col-md-5">
                                            <label for="email" class="form-label">Correo<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="usuario@ejemplo.com" required value="{{old('email')}}">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="password" class="form-label">Contraseña<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="password" class="form-control" id="password" name="password" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="gender" class="form-label">Género<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <select id="gender" class="form-select" name="gender" required>
                                                <option value="masculino" {{old('gender') == 'masculino' ? 'selected' : ''}}>Masculino</option>
                                                <option value="femenino" {{old('gender') == 'femenino' ? 'selected' : ''}}>Femenino</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="address" class="form-label">Dirección<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="text" class="form-control" id="address" name="address" placeholder="Avenida López" required value="{{old('address')}}">
                                        </div>   
                                        <div class="col-md-3">
                                            <label for="phone" class="form-label">Teléfono<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <input type="text" class="form-control" id="phone" name="phone" placeholder="0412......" required value="{{old('phone')}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="role" class="form-label">Rol<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                            <select id="role" class="form-select" name="role" required>
                                                <option value="admin" {{old('role') == 'admin' ? 'selected' : ''}}>Administrador</option>
                                                <option value="teacher" {{old('role') == 'teacher' ? 'selected' : ''}}>Profesor</option>
                                                <option value="student" {{old('role') == 'student' ? 'selected' : ''}}>Estudiante</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                        <label id="career_label" for="career_id" class="form-label" style="display:none">Selecciona una carrera<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                        <select id="career_id" class="form-select"  name="career_id" style="display:none">
                                                <option selected disabled>Seleccione una carrera</option>
                                            @foreach ($careers as $career)
                                                <option value="{{$career->id}}">{{$career->career_name}}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                        <div class="col-md-4">
                                        <label id="section_label" for="section_id" class="form-label" style="display:none">Selecciona una sección<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                        <select id="section_id" class="form-select"  name="section_id" style="display:none"></select>
                                        </div>
                                    </div>
                                    <button type="submit" class="mt-3 btn btn-sm btn-outline-success"><i class="bi bi-check2"></i> Guardar</button>
                                </form>
                                <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
                                <script>
                                    let role  = document.getElementById("role")
                                    let career = document.getElementById("career_id")
                                    let labelC = document.getElementById("career_label")
                                    let section = document.getElementById("section_id")
                                    let labelS = document.getElementById("section_label")

                                    role.addEventListener("change", () => {
                                    let eleccion = role.options[role.selectedIndex].text
                        
                                        if(eleccion === "Estudiante") {
                                            career.style.display = "inline"
                                            labelC.style.display="block"
                                            section.style.display = "inline"
                                            labelS.style.display= "block"

                                        } else {
                                            career.style.display = "none"
                                            labelC.style.display= "none"
                                            section.style.display = "none"
                                            labelS.style.display= "none"

                                        }
                                    });

                                    $('#career_id').change(function(){
                                        
                                        let careerID = $(this).val();

                                        if (careerID){

                                            $.ajax({
                                                type: 'GET',
                                                url: "{{ url('getSection') }}?career_id=" + careerID,
                                                success: function(res) {

                                                    if (res) {
                                                        $("#section_id").empty();
                                                        $("#section_id").append('<option>Seleciona una Sección</option>');
                                                        $.each(res, function(key, value) {
                                                            $("#section_id").append('<option value="' + key + '">' + value + '</option>');

                                                           
                                                        });

                                                    } else {

                                                        $("#section_id").empty();
                                                    }
                                                }
                                            });
                                        } 

                                    });

                                </script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection