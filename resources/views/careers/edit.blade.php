@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Editar Carrera</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{url()->previous()}}">Ver Carreras</a></li> 
                            <li class="breadcrumb-item active" aria-current="page">Editar Carrera</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="row">
                        <div class="col-md-5 mb-4">
                            <div class="p-3 border bg-light shadow">
                                <form action="{{ route('careers.update', $career->id) }}" method="POST">
                                @method ('PUT')
                                @csrf
                                    <div class="mb-4">
                                        <label for="career_name" class="form-label">Nombre de la Carrera: </label>
                                        <input type="text" class="form-control" id="career_name" name="career_name" placeholder="Introduzca el nombre de la carrera" value="{{ $career->career_name }}" required>
                                    </div>
                                    <button class="btn btn-sm btn-outline-success" type="submit"><i class="bi bi-check2"></i> Actualizar</button>
                                    <a class="btn btn-sm btn-outline-secondary" href="{{ route ('careers.index') }}"><i class="bi bi-arrow-counterclockwise"></i> Regresar</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection