@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Crear Carrera</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Crear Carrera</li>
                        </ol>
                    </nav>
                     @include('messages')
                    <div class="row">
                        <div class="col-md-5 mb-4">
                            <div class="p-3 border bg-light shadow">
                                <form action="{{ route('careers.store') }}" method="POST">
                                    @csrf
                                    <div class="mb-1">
                                        <label for="career_name" class="form-label">Nombre de la Carrera: </label>
                                        <input type="text" class="form-control" id="career_name" name="career_name" placeholder="Introduzca el nombre de la carrera" required>
                                    </div>
                                    <button type="submit" class="mt-3 btn btn-sm btn-outline-success"><i class="bi bi-check2"></i> Guardar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection