@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-person-lines-fill"></i> Pensum de {{$career->career_name}}</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Lista de Materias</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="mb-4 mt-4 col-md-8">
                        <div class="bg-white border shadow-sm p-3 mt-4">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">Materia</th>
                                        <th scope="col">Semestre</th>
                                        <th scope="col">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($subjects as $subject)
                                    @if ($subject->career_id == $career->id)
                                    <tr>
                                        <td>{{$subject->subject_name}}</td>
                                        <td>{{$subject->semesters->semester_name}}</td>
                                        <td>
                                        <a href="{{ route('subjects.edit', $subject->id)}}" role="button" class="btn btn-sm btn-outline-secondary"><i class="bi bi-pen"></i> Editar</a>
                                        </td> 
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--@i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection