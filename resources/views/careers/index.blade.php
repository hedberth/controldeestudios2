@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-diagram-3"></i> Carreras</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ver Carreras</li>
                        </ol> 
                    </nav>
                    <div class="row">
                        @foreach ($careers as $career)
                            @php
                                $total_sections = 0;
                            @endphp
                            <div class="col-11">
                                <div class="card my-3 shadow-sm">
                                    <div class="card-header bg-transparent">
                                        
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#class{{$career->id}}" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true"><i class="bi bi-diagram-3"></i> {{$career->career_name}}</button>
                                            </li>
                                            <li class="nav-item">
                                               <a style="text-decoration: none;" href="{{route('career.pensum', $career->id)}}"> <button class="nav-link" data-bs-toggle="tab" data-bs-target="#class{{$career->id}}-courses" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false"><i class="bi bi-journal-medical"></i> Pensum</button> </a>
                                            </li>
                                            <li class="nav-item">
                                               <a style="text-decoration: none;" href="{{route('section.index', $career->id)}}"> <button class="nav-link" data-bs-toggle="tab" data-bs-target="#class{{$career->id}}-courses" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false"><i class="bi bi-journal-medical"></i> Secciones</button> </a>
                                            </li>
                                            
                                        </ul>
                                        
                                        <div class="card-body text-dark">
                                            <div class="tab-content" id="myTabContent">
                                                
                                                <div class="tab-pane fade show active" id="class{{$career->id}}" role="tabpanel"> <!-- Carreras -->                                
                                                    <div class="accordion" id="accordionCareer{{$career->id}}">
                                                    @foreach ($sections as $section)
                                                        @if ($section->career_id == $career->id)
                                                            @php
                                                                $total_sections++;
                                                            @endphp

                                                           

                                                        @endif
                                                    @endforeach
                                                    </div>
                                                </div>

                                                
                                            </div>
                                        </div>

                                        <div class="card-footer bg-transparent d-flex justify-content-between">
                                            <span>Secciones: {{$total_sections}}</span>
                                            <span><a href="{{route ('careers.edit', $career->id) }}" class="btn btn-sm btn-outline-success" role="button"><i class="bi bi-pencil"></i> Editar Carrera</a></span>
                                        </div>                                        
                                     
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            {!! $careers->links('pagination::bootstrap-4') !!}
            <!-- @i('layouts.footer') --> 
        </div>
    </div>
</div>
@endsection
