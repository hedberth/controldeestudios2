@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Semestres Disponibles</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ver Semestre</li>
                        </ol>
                    </nav>
                    <div class="row">
                        <div class="col-md-7 mb-4">
                            <div class="p-3 border bg-light shadow-sm">
                                <table class="table table-striped table-hover">
                                    <thead class="thead">
                                        <tr>
                                            <th>Nombre del Semestre</th>
                                            

                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($semesters as $semester)
                                            <tr>
                                                
                                                <td>{{ $semester->semester_name }}</td>
            
                                                <td>
                                                    <a href="{{ route('semesters.edit', $semester->id)}}" class="btn btn-warning btn-sm " title="Modificar">
                                                        Modificar <i class="bi bi-pencil-square"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection
