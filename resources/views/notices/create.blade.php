@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-megaphone"></i> Crear Anuncio</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Crear Anuncio</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="row">
                        <form action="{{ route('notices.store')}}" method="POST">
                            @csrf
                            <input type="hidden" name="notice">
                            @include('components.ckeditor.editor', ['name' => 'notice'])
                            <button type="submit" class="btn btn-outline-success"><i class="bi bi-check2"></i> Publicar</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection