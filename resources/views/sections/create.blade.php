@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Crear Sección</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Crear Sección</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="row">
                        <div class="col-md-5 mb-4">
                            <div class="p-3 border bg-light shadow">
                                <form action="{{ route('sections.store')}}" method="POST">
                                    @csrf
                                    <div class="mb-4">
                                        <label for="section_name" class="form-label">Nombre de la Sección<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                        <input type="text" class="form-control" id="section_name" name="section_name" required>
                                    </div>
                                    <div class="mb-3">
                                        <p>Asignar sección a la carrera:<sup><i class="bi bi-asterisk text-primary"></i></sup></p>
                                        <select class="form-select" name="career_id" aria-label=".form-select" required>
                                            @foreach ($careers as $career)
                                                <option value="{{$career->id}}">{{$career->career_name}}</option>
                                            @endforeach
                                        </select>           
                                    </div>
                                    <div class="mb-3">
                                        <p>Asignar sección al semestre:<sup><i class="bi bi-asterisk text-primary"></i></sup></p>
                                        <select class="form-select" name="semester_id" aria-label=".form-select" required>
                                            @foreach ($semesters as $semester)
                                                <option value="{{$semester->id}}">{{$semester->semester_name}}</option>
                                            @endforeach
                                        </select>           
                                    </div>
                                    <button type="submit" class="mt-3 btn btn-sm btn-outline-success"><i class="bi bi-check2"></i> Guardar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection