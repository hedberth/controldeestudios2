@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        @include('layouts.left-menu')
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-person-lines-fill"></i>Secciones de {{$career->career_name}}</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="breadcrumb" href="{{route('home')}}">Menú Principal</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Lista de Secciones</li>
                        </ol>
                    </nav>
                    @include('messages')
                    <div class="mb-4 mt-4 col-md-10">
                        <div class="bg-white border shadow-sm p-3 mt-4">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Semestre</th>
                                        <th scope="col">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sections as $section)
                                    @if ($section->career_id == $career->id)
                                    <tr>
                                        <td>{{$section->section_name}}</td>
                                        <td>{{$section->semesters->semester_name}}</td>
                                        
                                        <td>
                                            <div>
                                                <a href="{{ route('section.list', $section->id)}}" role="button" class="btn btn-sm btn-outline-info"><i class="bi bi-eye"> Ver</i></a>
                                                <a href="{{ route('sections.edit', $section->id)}}" role="button" class="btn btn-sm btn-outline-secondary"><i class="bi bi-pen">Editar</i></a>
                                                <a href="{{ route('schedule.show', ['career'=> $career->id,'section'=> $section->id])}}" role="button" class="btn btn-sm btn-outline-secondary"><i class="bi bi-pen"></i> Horario</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--@i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection
