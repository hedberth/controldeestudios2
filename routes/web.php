<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('careers', App\Http\Controllers\CareerController::class);

Route::get('/careers/{career}/pensum', [App\Http\Controllers\CareerController::class, 'pensum'])->name('career.pensum');

//

Route::resource('sections', App\Http\Controllers\SectionController::class);

Route::get('/sections/{career}/index', [App\Http\Controllers\SectionController::class, 'index'])->name('section.index');

Route::get('/sections/{section}/list', [App\Http\Controllers\SectionController::class, 'list'])->name('section.list');

Route::resource('semesters', App\Http\Controllers\SemesterController::class);

Route::resource('subjects', App\Http\Controllers\SubjectController::class);

Route::resource('notices', App\Http\Controllers\NoticeController::class);

//

Route::resource('users', App\Http\Controllers\UserController::class);

Route::get('/users/view/change', [App\Http\Controllers\UserController::class, 'viewChange'])->name('user.viewChange');

Route::put('/users/view/cambio', [App\Http\Controllers\UserController::class, 'change'])->name('user.cambio');

Route::get('/users/view/{user}/record', [App\Http\Controllers\UserController::class, 'record'])->name('user.record');

Route::get('/constancia/pdf', [App\Http\Controllers\UserController::class, 'pdf'])->name('constancia.pdf');

Route::get('/carnet/pdf', [App\Http\Controllers\UserController::class, 'carnet'])->name('carnet.pdf');


#esta ruta (getSection) es para la funcion de javascript de section
Route::get('getSection',[App\Http\Controllers\UserController::class, 'getSection'])->name('getSection');

Route::get('getSubject',[App\Http\Controllers\UserController::class, 'getSubject'])->name('getSubject');

#rutas de inscripcion con (enrollmenst)
Route::get('/enrollments/create', [App\Http\Controllers\EnrollmentController::class, 'create'])->name('enrollment.create');

Route::post('/enrollments/store', [App\Http\Controllers\EnrollmentController::class, 'store'])->name('enrollment.store');

Route::get('/enrollments/view/{enrollment}/edit', [App\Http\Controllers\EnrollmentController::class, 'edit'])->name('enrollment.edit');

Route::put('/enrollments/view/{enrollment}', [App\Http\Controllers\EnrollmentController::class, 'update'])->name('enrollment.update');

#rutas para asignar a los profesores 

Route::get('/teachers/assigned', [App\Http\Controllers\AssignedTeacherController::class, 'assigned'])->name('teacher.assigned');

Route::post('/teachers/assigned', [App\Http\Controllers\AssignedTeacherController::class, 'store'])->name('teacher.store');

#esta ruta es la segunda fucion del store para la materia que daran los profesores
Route::post('/teachers/{assignedTeacher}', [App\Http\Controllers\AssignedTeacherController::class, 'storeTwo'])->name('teacher.storeTwo');

Route::get('/teachers/view/{user}/historic', [App\Http\Controllers\AssignedTeacherController::class, 'historic'])->name('teacher.historic');

Route::get('/teachers/view/{user}/section', [App\Http\Controllers\AssignedTeacherController::class, 'section'])->name('teacher.section');

Route::get('/teachers/view/{subject}/{section}/getStuden', [App\Http\Controllers\AssignedTeacherController::class, 'getStuden'])->name('teacher.getStuden');

#rutas para los horarios 
Route::get('/schedules/create', [App\Http\Controllers\ScheduleController::class, 'create'])->name('schedule.create');

Route::post('/schedules/create', [App\Http\Controllers\ScheduleController::class, 'store'])->name('schedule.store');

Route::get('/schedules/{career}/{section}', [App\Http\Controllers\ScheduleController::class, 'show'])->name('schedule.show');