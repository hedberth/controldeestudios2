<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->id();
            $table->string('section_name');
          

            $table->bigInteger('career_id')->unsigned();

            $table->bigInteger('semester_id')->nullable()->unsigned();
          
            $table->timestamps();

            $table->foreign('career_id')->references('id')->on('careers');

            $table->foreign('semester_id')->references('id')->on('semesters');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
};
