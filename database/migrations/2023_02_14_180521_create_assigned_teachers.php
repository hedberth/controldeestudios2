<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_teachers', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('user_id')->unsigned();

            $table->bigInteger('career_id')->unsigned();

            $table->bigInteger('section_id')->unsigned();

            $table->bigInteger('subject_id')->unsigned()->nullable();

            $table->foreign('user_id')->references('id')->on('users');

            $table->foreign('career_id')->references('id')->on('careers');

            $table->foreign('section_id')->references('id')->on('sections');

            $table->foreign('subject_id')->references('id')->on('subjects');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_teachers');
    }
};
