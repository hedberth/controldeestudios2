<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->id();
            $table->string('start');
            $table->string('end');
            $table->integer('weekday');

            $table->bigInteger('career_id')->unsigned();

            $table->bigInteger('section_id')->unsigned();

            $table->bigInteger('subject_id')->unsigned();

            $table->foreign('career_id')->references('id')->on('careers');

            $table->foreign('section_id')->references('id')->on('sections');

            $table->foreign('subject_id')->references('id')->on('subjects');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
};
