<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('ci');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('gender');
            $table->string('phone');
            $table->string('address');
            $table->string('birthdate');
            $table->string('role');
            $table->string('password');
            $table->rememberToken();

            $table->bigInteger('career_id')->nullable()->unsigned();

            $table->bigInteger('section_id')->nullable()->unsigned();

            $table->timestamps();

            $table->foreign('career_id')->references('id')->on('careers');

            $table->foreign('section_id')->references('id')->on('sections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
