<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Habraan',
            'last_name' => 'Tovar',
            'ci' => '12345678',
            'email' => 'admin@gmail.com',
            'password' => '123456789',
            'gender' => 'masculino',
            'phone' => '04125574856',
            'address' => 'Santa teresa, la tortuga',
            'birthdate' => '1998-06-24',
            'role' => 'admin',
        ]);

        User::create([
            'first_name' => 'Luis',
            'last_name' => 'Soto',
            'ci' => '12345678',
            'email' => 'luis@gmail.com',
            'password' => 'password',
            'gender' => 'masculino',
            'phone' => '04125574856',
            'address' => 'Santa teresa, la tortuga',
            'birthdate' => '1998-06-24',
            'role' => 'teacher',
        ]);

        User::create([
            'first_name' => 'Maribel',
            'last_name' => 'Garcia',
            'ci' => '12345678',
            'email' => 'user@gmail.com',
            'password' => 'password',
            'gender' => 'femenino',
            'phone' => '04125574856',
            'address' => 'Santa teresa, la tortuga',
            'birthdate' => '1998-06-24',
            'role' => 'student',
            'career_id' => '1',
            'section_id' => '1',
        ]);
    }
}
