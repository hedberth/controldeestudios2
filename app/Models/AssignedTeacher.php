<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Section;

class AssignedTeacher extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','career_id','section_id','subject_id'];

    public function users(){

        return $this->belongsTo(User::class, 'user_id');
    }
    public function careers(){

        return $this->belongsTo(Career::class, 'career_id');
    }
    public function sections(){

        return $this->belongsTo(Section::class, 'section_id');
    }
    public function subjects(){

        return $this->belongsTo(Subject::class, 'subject_id');
    }
}
