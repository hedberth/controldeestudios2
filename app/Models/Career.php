<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Section;
use App\Models\Subject;
use App\Models\User;

class Career extends Model
{
    use HasFactory;

     protected $fillable = ['career_name'];

     public function sections()
     {
        return $this->hasMany(Section::class, 'id');
     }

     public function subjects()
     {
        return $this->hasMany(Subject::class, 'id');
     }

     public function users()
     {
        return $this->hasMany(User::class, 'id');
     }
}
