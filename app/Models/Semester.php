<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Subject;
use App\Models\Section;

class Semester extends Model
{
    use HasFactory;

    protected $fillable = ['semester_name', 'start_date', 'end_date'];

    public function subjects()
    {
        return $this->hasMany(Subject::class, 'id');
    }
    public function sections()
    {
        return $this->hasMany(Section::class, 'id');
    }

}
