<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;

    protected $fillable = ['career_id', 'section_id','subject_id','weekday','start','end'];

    public function subjects(){

        return $this->belongsTo(Subject::class, 'subject_id');
    }
}
