<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Career;
use App\Models\Semester;

class Subject extends Model
{
    use HasFactory;

    protected $fillable = ['subject_name',  'career_id', 'semester_id'];

    public function careers() 
    {
        return $this->belongsTo(Career::class, 'career_id');
    }

    public function semesters() 
    {
        return $this->belongsTo(Semester::class, 'semester_id');
    }
    
}
