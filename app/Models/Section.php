<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Career;
use App\Models\Semester;
use App\Models\User;

class Section extends Model
{
    use HasFactory;

    protected $fillable = ['section_name', 'career_id','semester_id'];

    public function careers()
    {
        return $this->belongsTo(Career::class, 'career_id');
    }
    public function semesters()
    {
        return $this->belongsTo(Semester::class, 'semester_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'id');
    }
    
}
