<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Career;
use App\Models\Section;
use App\Models\Enrollment;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
  
    public function index(User $user)
    {

        $this->authorize('viewAny', $user);

        $users = User::where("role","!=","student")->get();

        return view('users.index', compact('users'));
    }

    //esta duncion sera para mostrar un pdf 

    public function pdf(){
      
        $date = Carbon::now();

        $pdf = Pdf::loadView('users.constanciapdf', compact('date'));
        return $pdf->stream();
        // return view('users.constanciapdf', compact('date'));

    }

    public function carnet(){
      
        // $date = Carbon::now();

        // $pdf = Pdf::loadView('users.carnetpdf');
        // return $pdf->stream();
        return view('users.carnetpdf');

    }
   
    public function create(User $user)
    {
        $this->authorize('create', $user);

        $user = new User();
        $careers = Career::all();
        $sections = Section::all();
        
        return view('users.create', compact('user','careers', 'sections'));
    }

   
    public function store(Request $request, User $user)
    {
        $this->authorize('create', $user);

        $user = User::create($request->all());
        
        return back()->with('status', 'El usuario fue creado exitosamente!');
    }

  
    public function show(User $user)
    {
        
        $careers = Career::all();

        return view('users.show', compact('user','careers'));
    }

 
    public function edit(User $user)
    {
        $this->authorize('update', $user);

        return view('users.edit', compact('user'));
    }

  
    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);

        $user->update($request->all());

        return back()->with('status', 'el usuario fue actualizado exitosamente!');

    }


    public function viewChange(User $user)
    {
        return view('users.viewChange', compact('user'));

    }

    public function Change(Request $request)
    {
        $request->validate([
            'password' => 'required|confirmed|min:8',
        ]);
        
        // $auth = Auth::user();
        // $user_id = $auth->id;

        $user = User::find(Auth::id());
        $user->password =  $request->password;
        $user->update();

        return back()->with('status', 'tu contraseña fue actulaiczada exitosamente!');

    }

   
    public function destroy($id)
    {
        //
    }
    
    
    
    public function record(User $user)
    {
        $enrollments = Enrollment::all();

        return view('users.record', compact('user','enrollments'));
    }
    
    #esta funcion es para (section js)
    public function getSection(Request $request)
    {
        $sections = DB::table("sections")->where('career_id', $request->career_id)->pluck('section_name', 'id');

        return response()->json($sections);
    }
    #esta funcion es para traer los datos (nombre) de la materias cuando le das a carreras(subject js)
    public function getSubject(Request $request)
    {
        $subjects = DB::table("subjects")->where('career_id', $request->career_id)->pluck('subject_name', 'id');

        return response()->json($subjects);
    }
}
