<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AssignedTeacher;
use App\Models\User;
use App\Models\Career;
use App\Models\Section;
use App\Models\Semester;
use App\Models\Subject;
use App\Models\Enrollment;

class AssignedTeacherController extends Controller
{
    
    public function index()
    {
        //
    }

    
    public function assigned()
    {
        $users = User::all();
        $careers = Career::all();
        $sections = Section::all();

        return view('teachers.assigned', compact('users','careers','sections'));
    }

   
    public function store(Request $request)
    {
        $subjects = Subject::all();

        $assignedTeacher = new AssignedTeacher();

        $assignedTeacher->user_id =  $request->user_id;
        $assignedTeacher->career_id =   $request->career_id;
        $assignedTeacher->section_id =   $request->section_id;

        $assignedTeacher->save();
       
        
        return view('teachers.assignedTwo', compact('assignedTeacher','subjects'));
    }

    public function storeTwo(Request $request, AssignedTeacher $assignedTeacher )
    {
        
        $assignedTeacher->subject_id =   $request->subject_id;

        $assignedTeacher->save();
       
        
        return back()->with('status', 'El profesor fue asignado exitosamente!');
    }


   
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }

    public function historic(User $user){

    $assignedTeachers = AssignedTeacher::all();

    return view('teachers.historic', compact('assignedTeachers','user'));

    }

    public function section(User $user){

        $assignedTeachers = AssignedTeacher::all();
    
        return view('teachers.section', compact('assignedTeachers','user'));
    
    }

    public function getStuden(Subject $subject, Section $section){

        $enrollments = Enrollment::all();
    
        return view('teachers.getStuden', compact('subject','enrollments','section'));
    
    }

    

}
