<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Schedule;
use App\Models\Career;
use App\Models\Section;
use App\Models\Subject;

class ScheduleController extends Controller
{
    
    public function index()
    {
        //
    }

    
    public function create()
    {
        $careers = Career::all();
        $sections = Section::all();
        $subjects = Subject::all();

        return view('schedules.create', compact('careers','sections','subjects'));
    }

    
    public function store(Request $request)
    {
        $schedules = Schedule::create($request->all());
        
        return back()->with('status', 'El Horario fue creado exitosamente!');
    }

    public function getAll($career, $section) {
        return Schedule::with('subjects')->where('career_id', $career)->where('section_id', $section)->get();
    }

    public function show($career, $section)
    {
        $schedules = self::getAll($career, $section);
        $schedules = $schedules->sortBy('weekday')->groupBy('weekday');

        $data = [
            'schedules' => $schedules
        ];

        return view('schedules.show', $data);
    }

    
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        //
    }
}
