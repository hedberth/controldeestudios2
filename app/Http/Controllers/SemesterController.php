<?php

namespace App\Http\Controllers;
use App\Models\Semester;

use Illuminate\Http\Request;

class SemesterController extends Controller
{
    
    public function index()
    {
        $semesters = Semester::all();

        return view ('semesters.index', compact('semesters'));
    }

    
    public function create()
    {
        $semester = new Semester();

        return view('semesters.create', compact('semester'));


    }

    
    public function store(Request $request)
    {
        $semester = Semester::create($request->all());

    
        return back()->with('status', 'El semestre fue creado exitosamente!');

    }

    public function edit($id)
    {
        $semester = Semester::find($id);
        
        return view('semesters.edit', compact('semester'));
    }

   
    public function update(Request $request, Semester $semester)
    {
        $semester->update($request->all());

        return back()->with('status', 'El semestre fue actualizado exitosamente!');
    }
}
