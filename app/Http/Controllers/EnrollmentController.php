<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Enrollment;
use App\Models\Subject;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class EnrollmentController extends Controller
{
    
    public function index()
    {
        //
    }

   
    public function create()
    {
        // $subjects = Subject::all();
        $subjects = DB::select("select * from subjects where id 
        not in(select subject_id from enrollments where  user_id = ?)", [auth::user()->id]);
        return view('enrollments.create', compact('subjects'));
    }

   
    public function store(Request $request)
    {
        $user_id =   $request->user_id;
        $subject_id = $request->subject_id;
        $section_id = $request->section_id;

        for($i=0;$i<count($subject_id);$i++)
        {
            $datasave = [

                'user_id'   =>$user_id,
                'subject_id' =>$subject_id[$i],
                'section_id' =>$section_id,
    
            ];
    
            //return dd($datasave);
            DB::table('enrollments')->insert($datasave);
        }
            return back()->with('status', 'Te inscribiste Exitosamente');
        
    }
    
    public function show($id)
    {
        //
    }

   
    public function edit(Enrollment $enrollment)
    {
        return view('enrollments.edit', compact('enrollment'));
    }

   #funcion para evaluar 
    public function update(Request $request, Enrollment $enrollment)
    {
        $enrollment->mark = $request->mark;
        $enrollment->save();

        return back()->with('status', 'El alumno fue evaluado');
    }

   
    public function destroy($id)
    {
        //
    }
}
