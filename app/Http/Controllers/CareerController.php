<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Career;
use App\Models\Section;
use App\Models\Subject;
class CareerController extends Controller
{
   
    public function index(Career $career,Request $request)
    {
        $this->authorize('viewAny', $career);

        $subjects = Subject::all();
        $sections = Section::all();
        $careers = Career::orderBy('career_name', 'asc')->paginate(3);

        return view('careers.index', compact('careers', 'sections', 'subjects'));
    }

    public function create(Career $career)
    {
        $this->authorize('create', $career);

        $career = new Career();
        
        return view('careers.create', compact('career'));
    }

    
    public function store(Request $request)
    {
        $career = Career::create($request->all());
        
        return back()->with('status', 'La carrera fue creada exitosamente!');
    }

    public function show($id)
    {
        //
    }

   
    public function edit(Career $career)
    {
        $this->authorize('viewAny', $career);

        #$career = Career::find($id);
        
        return view('careers.edit', compact('career'));
    }

   
    public function update(Request $request, Career $career)
    {
        $career->update($request->all());

        return back()->with('status', 'La carrera fue actualizada exitosamente!');

    }

    public function destroy($id)
    {
        //
    }
    
    public function pensum(Career $career)
    {
        $subjects = Subject::all();

        return view('careers.pensum', compact('subjects','career'));
    }
}
